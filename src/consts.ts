export const BUS_NAME = `org.freedesktop.UDisks2`;

export const BLOCK_DEVICE_INTERFACE = `org.freedesktop.UDisks2.Block`;
export const FILE_SYSTEM_INTERFACE = `org.freedesktop.UDisks2.Filesystem`;
export const DRIVE_INTERFACE = `org.freedesktop.UDisks2.Drive`;
export const OBJECT_MANAGER_INTERFACE = `org.freedesktop.DBus.ObjectManager`;
