import {
  systemBus,
  MessageBus,
  ProxyObject,
  ClientInterface,
  Variant,
} from "dbus-next";
import { on } from "events";
import BlockDevice from "./blockDevice";
import {
  BLOCK_DEVICE_INTERFACE,
  BUS_NAME,
  OBJECT_MANAGER_INTERFACE,
} from "./consts";
import { readProperty } from "./properties";
import {
  constructDbusObject,
  DbusObject,
  StandardOptions,
} from "./standardOptions";

const ROOT_OBJECT = `/org/freedesktop/UDisks2`;
const MANAGER_OBJECT = `/org/freedesktop/UDisks2/Manager`;
const MANAGER_INTERFACE = `org.freedesktop.UDisks2.Manager`;

interface ManagerInterface extends ClientInterface {
  GetBlockDevices(options: DbusObject): Promise<string[]>;
}

class Udisks2 {
  readonly #dbus: MessageBus = systemBus();
  readonly #rootObject: Promise<ProxyObject> = this.#dbus.getProxyObject(
    BUS_NAME,
    ROOT_OBJECT
  );
  readonly #managerObject: Promise<ProxyObject> = this.#dbus.getProxyObject(
    BUS_NAME,
    MANAGER_OBJECT
  );

  // todo: canFormat
  // todo: camResize
  // todo: canCheck
  // todo: canRepair
  // todo: loopSetup
  // todo: mdRaidCreate
  // todo: enableModules

  public async getBlockDevices(
    options: StandardOptions = {}
  ): Promise<BlockDevice[]> {
    const dbusOptions = constructDbusObject({
      no_user_authentication: {
        value: options.noUserAuthentication,
        type: `b`,
      },
    });

    const managerObject = await this.#managerObject;
    const manager = managerObject.getInterface<ManagerInterface>(
      MANAGER_INTERFACE
    );
    const blockDevicesPaths = await manager.GetBlockDevices(dbusOptions);
    const blockDevices = await Promise.all(
      blockDevicesPaths.map(
        async (path) =>
          new BlockDevice(
            this.#dbus,
            await this.#dbus.getProxyObject(BUS_NAME, path),
            await this.#rootObject
          )
      )
    );

    return blockDevices;
  }

  public async *watchAddedBlockDevices(): AsyncGenerator<BlockDevice, void> {
    const rootObject = await this.#rootObject;
    const root = rootObject.getInterface(OBJECT_MANAGER_INTERFACE);

    const events: AsyncIterableIterator<
      [
        string,
        { [interfaceName: string]: { [property: string]: Variant<unknown> } }
      ]
    > = on(root, `InterfacesAdded`);

    for await (const [objectPath, interfaces] of events) {
      if (BLOCK_DEVICE_INTERFACE in interfaces) {
        const object = await this.#dbus.getProxyObject(BUS_NAME, objectPath);
        yield new BlockDevice(this.#dbus, object, rootObject);
      }
    }
  }

  // todo: resolveDeivce

  public async version(): Promise<string> {
    return readProperty(
      await this.#managerObject,
      MANAGER_INTERFACE,
      `Version`
    );
  }

  public async supportedFilesystems(): Promise<string[]> {
    return readProperty(
      await this.#managerObject,
      MANAGER_INTERFACE,
      `SupportedFilesystems`
    );
  }

  public async supportedEncryptionTypes(): Promise<string[]> {
    return readProperty(
      await this.#managerObject,
      MANAGER_INTERFACE,
      `SupportedEncryptionTypes`
    );
  }

  public async defaultEncryptionType(): Promise<string> {
    return readProperty(
      await this.#managerObject,
      MANAGER_INTERFACE,
      `DefaultEncryptionType`
    );
  }
}

export default Udisks2;
