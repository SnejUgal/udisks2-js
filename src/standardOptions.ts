import { Variant } from "dbus-next";

export interface StandardOptions {
  noUserAuthentication?: boolean;
}

interface Properties {
  [key: string]: { value?: unknown; type: string };
}

export interface DbusObject {
  [key: string]: Variant<unknown>;
}

export function constructDbusObject(properties: Properties): DbusObject {
  const object: DbusObject = {};

  for (const [key, { value, type }] of Object.entries(properties)) {
    if (value === undefined) {
      continue;
    }

    object[key] = new Variant(type, value);
  }

  return object;
}
