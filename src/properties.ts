import { ClientInterface, ProxyObject, Variant } from "dbus-next";
import { on } from "events";

const PROPERTIES_INTERFACE = `org.freedesktop.DBus.Properties`;

interface PropertiesInterface extends ClientInterface {
  Get<T>(interfaceName: string, property: string): Promise<Variant<T>>;
}

export async function readProperty<T>(
  object: ProxyObject,
  interfaceName: string,
  property: string
): Promise<T> {
  const properties = object.getInterface<PropertiesInterface>(
    PROPERTIES_INTERFACE
  );
  const variant = await properties.Get<T>(interfaceName, property);
  return variant.value;
}

export async function* watchProperty<T>(
  object: ProxyObject,
  interfaceName: string,
  property: string
): AsyncGenerator<T, void> {
  const properties = object.getInterface<PropertiesInterface>(
    PROPERTIES_INTERFACE
  );

  const events: AsyncIterableIterator<
    [string, { [key: string]: Variant }, string[]]
  > = on(properties, `PropertiesChanged`);

  for await (const [changedInterface, changed, invalidated] of events) {
    if (changedInterface !== interfaceName) {
      continue;
    }

    if (invalidated.includes(property)) {
      throw new Error(`${property} got invalidated`);
    }

    if (property in changed) {
      yield changed[property].value;
    }
  }
}
