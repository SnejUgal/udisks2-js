import { ClientInterface, ProxyObject } from "dbus-next";
import { FILE_SYSTEM_INTERFACE } from "./consts";
import { readProperty, watchProperty } from "./properties";
import {
  constructDbusObject,
  DbusObject,
  StandardOptions,
} from "./standardOptions";

interface FileSystemInterface extends ClientInterface {
  Mount(options: DbusObject): Promise<string>;
  Unmount(options: DbusObject): Promise<void>;
}

export interface MountOptions extends StandardOptions {
  fsType?: string;
  options?: string;
}

export interface UnmountOptions extends StandardOptions {
  force?: boolean;
}

class FileSystem {
  readonly #object: ProxyObject;

  constructor(object: ProxyObject) {
    this.#object = object;
  }

  // todo: setLabel

  public async mount(options: MountOptions = {}): Promise<String> {
    const dbusOptions = constructDbusObject({
      no_user_authentication: {
        value: options.noUserAuthentication,
        type: `b`,
      },
      fstype: { value: options.fsType, type: `s` },
      options: { value: options.options, type: `s` },
    });

    const fileSystem = this.#object.getInterface<FileSystemInterface>(
      FILE_SYSTEM_INTERFACE
    );

    return fileSystem.Mount(dbusOptions);
  }

  public async unmount(options: UnmountOptions = {}): Promise<void> {
    const dbusOptions = constructDbusObject({
      no_user_authentication: {
        value: options.noUserAuthentication,
        type: `b`,
      },
      force: { value: options.force, type: `b` },
    });

    const fileSystem = this.#object.getInterface<FileSystemInterface>(
      FILE_SYSTEM_INTERFACE
    );

    await fileSystem.Unmount(dbusOptions);
  }

  // todo: resize
  // todo: check
  // todo: repair
  // todo: takeOwnership

  public mountPoints(): Promise<Buffer[]> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `MountPoints`);
  }

  public watchMountPoints(): AsyncIterableIterator<Buffer[]> {
    return watchProperty(this.#object, FILE_SYSTEM_INTERFACE, `MountPoints`);
  }

  public size(): Promise<number> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Size`);
  }
}

export default FileSystem;
