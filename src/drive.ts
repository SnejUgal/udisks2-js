import { ClientInterface, ProxyObject } from "dbus-next";
import { DRIVE_INTERFACE, FILE_SYSTEM_INTERFACE } from "./consts";
import { readProperty } from "./properties";
import {
  constructDbusObject,
  DbusObject,
  StandardOptions,
} from "./standardOptions";

type MediaCompatibility =
  | `thumb`
  | `flash`
  | `flash_cf`
  | `flash_ms`
  | `flash_sm`
  | `flash_sd`
  | `flash_sdhc`
  | `flash_sdxc`
  | `flash_mmc`
  | `floppy`
  | `floppy_zip`
  | `floppy_jaz`
  | `optical`
  | `optical_cd`
  | `optical_cd_r`
  | `optical_cd_rw`
  | `optical_dvd`
  | `optical_dvd_r`
  | `optical_dvd_rw`
  | `optical_dvd_ram`
  | `optical_dvd_plus_r`
  | `optical_dvd_plus_rw`
  | `optical_dvd_plus_r_dl`
  | `optical_dvd_plus_rw_dl`
  | `optical_bd`
  | `optical_bd_r`
  | `optical_bd_re`
  | `optical_hddvd`
  | `optical_hddvd_r`
  | `optical_hddvd_rw`
  | `optical_mo`
  | `optical_mrw`
  | `optical_mrw_w`;

type ConnectionBus = `usb` | `sdio` | `ieee1394`;

interface DriveInterface extends ClientInterface {
  Eject(options: DbusObject): Promise<void>;
}

class Drive {
  readonly #object: ProxyObject;

  constructor(object: ProxyObject) {
    this.#object = object;
  }

  public async eject(options: StandardOptions = {}): Promise<void> {
    const dbusOptions = constructDbusObject({
      no_user_authentication: {
        value: options.noUserAuthentication,
        type: `b`,
      },
    });

    const drive = this.#object.getInterface<DriveInterface>(DRIVE_INTERFACE);

    await drive.Eject(dbusOptions);
  }

  // todo: setConfiguration
  // todo: powerOff

  public async vendor(): Promise<string> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Vendor`);
  }

  public async model(): Promise<string> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Model`);
  }

  public async revision(): Promise<string> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Revision`);
  }

  public async serial(): Promise<string> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Serial`);
  }

  public async worldWideName(): Promise<string> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `WWN`);
  }

  public async id(): Promise<string> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Id`);
  }

  // todo: configuration

  public async media(): Promise<string> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Media`);
  }

  public async mediaCompatibility(): Promise<MediaCompatibility> {
    return readProperty(
      this.#object,
      FILE_SYSTEM_INTERFACE,
      `MediaCompatibility`
    );
  }

  public async mediaRemovable(): Promise<boolean> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `MediaRemovable`);
  }

  public async mediaAvailable(): Promise<boolean> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `MediaAvailable`);
  }

  public async mediaChangeDelected(): Promise<boolean> {
    return readProperty(
      this.#object,
      FILE_SYSTEM_INTERFACE,
      `MediaChangeDetected`
    );
  }

  public async size(): Promise<number> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Size`);
  }

  public async timeDetected(): Promise<Date> {
    const timestamp =
      (await readProperty<number>(
        this.#object,
        FILE_SYSTEM_INTERFACE,
        `TimeDetected`
      )) / 1000;
    return new Date(timestamp);
  }

  public async timeMediaDetected(): Promise<Date> {
    const timestamp =
      (await readProperty<number>(
        this.#object,
        FILE_SYSTEM_INTERFACE,
        `TimeMediaDetected`
      )) / 1000;
    return new Date(timestamp);
  }

  public async optical(): Promise<boolean> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Optical`);
  }

  public async opticalBlank(): Promise<boolean> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `OpticalBlank`);
  }

  public async opticalNumTracks(): Promise<number> {
    return readProperty(
      this.#object,
      FILE_SYSTEM_INTERFACE,
      `OpticalNumTracks`
    );
  }

  public async opticalNumAudioTracks(): Promise<number> {
    return readProperty(
      this.#object,
      FILE_SYSTEM_INTERFACE,
      `OpticalNumAudioTracks`
    );
  }

  public async opticalNumDataTracks(): Promise<number> {
    return readProperty(
      this.#object,
      FILE_SYSTEM_INTERFACE,
      `OpticalNumDataTracks`
    );
  }

  public async opticalNumSessions(): Promise<number> {
    return readProperty(
      this.#object,
      FILE_SYSTEM_INTERFACE,
      `OpticalNumSessions`
    );
  }

  public async rotationRate(): Promise<number> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `RotationRate`);
  }

  public async connectionBus(): Promise<ConnectionBus> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `ConnectionBus`);
  }

  public async seat(): Promise<string> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Seat`);
  }

  public async removable(): Promise<boolean> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Removable`);
  }

  public async ejectable(): Promise<boolean> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `Ejectable`);
  }

  public async sortKey(): Promise<string> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `SortKey`);
  }

  public async canPowerOff(): Promise<boolean> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `CanPowerOff`);
  }

  public async siblingId(): Promise<string> {
    return readProperty(this.#object, FILE_SYSTEM_INTERFACE, `SiblingId`);
  }
}

export default Drive;
