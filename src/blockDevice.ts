import { MessageBus, ProxyObject } from "dbus-next";
import { on } from "events";
import {
  BLOCK_DEVICE_INTERFACE,
  BUS_NAME,
  FILE_SYSTEM_INTERFACE,
  OBJECT_MANAGER_INTERFACE,
} from "./consts";
import Drive from "./drive";
import FileSystem from "./fileSystem";
import { readProperty } from "./properties";

export type IdUsage = `filesystem` | `crypto` | `raid` | `other` | ``;

class BlockDevice {
  readonly #dbus: MessageBus;
  readonly #object: ProxyObject;
  readonly #root: ProxyObject;

  constructor(dbus: MessageBus, object: ProxyObject, root: ProxyObject) {
    this.#dbus = dbus;
    this.#object = object;
    this.#root = root;
  }

  public async device(): Promise<Buffer> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `Device`);
  }

  public async preferredDevice(): Promise<Buffer> {
    return readProperty(
      this.#object,
      BLOCK_DEVICE_INTERFACE,
      `PreferredDevice`
    );
  }

  public async symlinks(): Promise<Buffer[]> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `Symlinks`);
  }

  public async deviceNumber(): Promise<number> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `DeviceNumber`);
  }

  public async id(): Promise<string> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `Id`);
  }

  public async size(): Promise<number> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `Size`);
  }

  public async readOnly(): Promise<boolean> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `ReadOnly`);
  }

  public async drive(): Promise<Drive> {
    const driveObjectPath = await readProperty<string>(
      this.#object,
      BLOCK_DEVICE_INTERFACE,
      `Drive`
    );
    const object = await this.#dbus.getProxyObject(BUS_NAME, driveObjectPath);
    return new Drive(object);
  }

  // todo: mdRaid
  // todo: mdRaidMember

  public async idUsage(): Promise<IdUsage> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `IdUsage`);
  }

  public async idType(): Promise<string> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `IdType`);
  }

  public async idVersion(): Promise<string> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `IdVersion`);
  }

  public async idLabel(): Promise<string> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `IdLabel`);
  }

  public async idUuid(): Promise<string> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `IdUUID`);
  }

  // todo: configuration

  public async cryptoBackingDeviceObjectPath(): Promise<string> {
    return readProperty(
      this.#object,
      BLOCK_DEVICE_INTERFACE,
      `CryptoBackingDeviceObjectPath`
    );
  }

  public async hintPartitionable(): Promise<boolean> {
    return readProperty(
      this.#object,
      BLOCK_DEVICE_INTERFACE,
      `HintPartitionable`
    );
  }

  public async hintSystem(): Promise<boolean> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `HintSystem`);
  }

  public async hintIgnore(): Promise<boolean> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `HintIgnore`);
  }

  public async hintAuto(): Promise<boolean> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `HintAuto`);
  }

  public async hintName(): Promise<string> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `HintName`);
  }

  public async hintIconName(): Promise<string> {
    return readProperty(this.#object, BLOCK_DEVICE_INTERFACE, `HintIconName`);
  }

  public async hintSymbolicIconName(): Promise<string> {
    return readProperty(
      this.#object,
      BLOCK_DEVICE_INTERFACE,
      `HintSymbolicIconName`
    );
  }

  public async userspaceMountOptions(): Promise<string[]> {
    return readProperty(
      this.#object,
      BLOCK_DEVICE_INTERFACE,
      `UserspaceMountOptions`
    );
  }

  public asFileSystem(): FileSystem | null {
    if (FILE_SYSTEM_INTERFACE in this.#object.interfaces) {
      return new FileSystem(this.#object);
    }

    return null;
  }

  public async awaitRemoval(): Promise<void> {
    const root = this.#root.getInterface(OBJECT_MANAGER_INTERFACE);

    const events: AsyncIterableIterator<[string, string[]]> = on(
      root,
      `InterfacesRemoved`
    );

    for await (const [objectPath, interfaces] of events) {
      if (
        objectPath == this.#object.path &&
        interfaces.includes(BLOCK_DEVICE_INTERFACE)
      ) {
        return;
      }
    }
  }
}

export default BlockDevice;
